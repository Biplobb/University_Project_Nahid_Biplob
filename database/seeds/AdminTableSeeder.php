<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $normaluser=\App\Role::where('name','supportadmin')->first();
        $superadmin=\App\Role::where('name','superadmin')->first();

        $user=new \App\Admin();
        $user->name='Support admin';
        $user->email='supportadmin@gmail.com';
        $user->phone='01874680376';
        $user->nid='445156484215451';
        $user->photo='9043.png';
        $user->password=bcrypt(123456);
        $user->save();
        $user->roles()->attach($normaluser);


        $user1=new \App\Admin();
        $user1->name='Super admin';
        $user1->email='superadmin@gmail.com';
        $user1->phone='01874680376';
        $user1->nid='445156484215451';
        $user1->photo='9043.png';
        $user1->password=bcrypt(123456);
        $user1->save();
        $user1->roles()->attach($superadmin);
    }
}
