<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicaltourismTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicaltourism', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hospital',255);
            $table->string('country',255);
            $table->string('branch',3000);
            $table->string('address',3000);
            $table->string('description',3000);
            $table->string('overview',3000);
            $table->string('accredition',3000);
            $table->longText('service')->nullable();
            $table->string('photo');
            $table->string('logo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicaltourism');
    }
}
