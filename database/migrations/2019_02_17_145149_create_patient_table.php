<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient', function (Blueprint $table) {



            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('address');
            $table->string('photo');

            $table->string('phone');
            $table->string('nid');
            $table->string('occupation');
            $table->string('status');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient');
    }
}
