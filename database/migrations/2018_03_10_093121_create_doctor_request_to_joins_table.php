<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorRequestToJoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_request_to_joins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('designation');
            $table->string('specialty');
            $table->string('gender');
            $table->string('phone')->unique();
            $table->string('location');
            $table->string('bmdc_reg_no')->unique();
            $table->string('nid')->unique();
            $table->string('degree');
            $table->string('experience');
            $table->string('interested-at');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_request_to_joins');
    }
}
