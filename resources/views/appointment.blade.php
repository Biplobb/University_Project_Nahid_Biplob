@extends('layouts.master')

@section('content')

    <!--Bread Crumb-->
    <section id="breadcrumb" class="space light-overlay" data-stellar-background-ratio="0.4">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bread-block">
                    <h2>Make An Appointment</h2>
                    <p>Affordable Treatments, Honest & Experienced Dentists.</p>
                </div>
            </div>
        </div>
    </section>
    <!--About US-->
    <section id="about-us" class="space-top bg-color v2">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 about-block no-padding">
                    <form action="#" method="post">
                        <div class="form-group col-sm-6">
                            <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                        </div>
                        <div class="form-group col-sm-6">
                            <input type="tel" class="form-control" name="" id="phone-number" placeholder="Phone Number">
                        </div>
                        <div class="form-group col-sm-6">
                            <select class="form-control selectpicker" name="location">
                                <option value="">Select Location</option>
                                <option value="pakistan">pakistan</option>
                                <option value="india">India</option>
                                <option value="australia">Australia</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <select class="form-control selectpicker" name="doctor">
                                <option value="">Choose Doctor</option>
                                <option value="Jhon Deo">Jhon Deo</option>
                                <option value="Jhon Deo">Jhon Deo</option>
                                <option value="Jhon Deo">Jhon Deo</option>
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <div class="col-sm-12 no-padding input input-group date">
                                <input type="text" class="form-control" id="appointment-date" placeholder="Appointment Date">
                                <label class="input-group-addon" for="departure">
                                    <i class="fa fa-angle-down"></i>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-12 book-appointment">
                            <button type="submit" href="#" class="btn"><span>BOOK APPOINTMENT</span>Experienced Dentists You Can Trust</button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-4 col-sm-offset-2 about-block text-right">
                    <img src="{{asset('front-end/assets/images/why.png')}}" alt="Columba">
                </div>
            </div>
        </div>
    </section>
    <!--Action-->
    <section class="action-8 space">
        <div class="container">
            <div class="row">
                <div class="action-block col-sm-12">
                    <div class="inner">
                        <div class="action-item">
                            <div class="icon">
                                <img src="{{asset('front-end/assets/images/topbar-icon-1.png')}}" alt="Columba">
                            </div>
                            <div class="action-text">
                                <h5>Monday - Friday : 08:00-19:00</h5>
                                <p>Saturday and Sunday - Closed</p>
                            </div>
                        </div>
                        <div class="action-item">
                            <div class="icon">
                                <img src="{{asset('front-end/assets/images/topbar-icon-2.png')}}" alt="Columba">
                            </div>
                            <div class="action-text">
                                <h5>1-800-700-6200</h5>
                                <p>themesfoundry@gmail.com</p>
                            </div>
                        </div>
                        <div class="action-item">
                            <div class="icon">
                                <img src="{{asset('front-end/assets/images/topbar-icon-3.png')}}" alt="Columba">
                            </div>
                            <div class="action-text">
                                <h5>121 King Street</h5>
                                <p>Melbourne</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection