

@extends('hospital-admin.layouts.master1')


{{--@include('admin.layouts.header')--}}
@section('content')

    <section class="content home">

        <div class="container-fluid">
            <div class="block-header">
                {{-- <div class="titless" style="overflow:hidden">
                     <div class="title" align="center"  >
                         {!! Form::open(['route' => 'searchPDFdoctor','method'=>'post','class'=>'class_name']) !!}
                         {!! Form::text('title', null, array('placeholder' => '        Search Text','id'=>'search_text')) !!}

                         <button class="clickable" id="clickable">.        search    .</button>

                         --}}{{--{!! Form::submit('search') !!}--}}{{--
                         {!! Form::close() !!}
                     </div>
                 </div>--}}
                <h2>Dashboard</h2>
                <small class="text-muted">Welcome to Swift application</small>

            </div>
            <h1>Doctor name search and show total serial</h1>
            {!! Form::open(['route' => 'serialsearch','method'=>'post','class'=>'class_name']) !!}
            {!! Form::text('title', null, array('placeholder' => '        Search Text','id'=>'search_text')) !!}
            <input type="hidden" name="re" value="{{$re}}" >
            <input type="date" name="date">
            <button class="clickable" id="clickable">.        search    .</button>

            {{--{!! Form::submit('search') !!}--}}
            {!! Form::close() !!}
            <div class="">

                <table class="table">
                    <thead>


                    <tr>
                        <th scope="col">Sl.</th>
                        <th scope="col">Patient Name</th>

                        <th scope="col">Doctor Name</th>
                        <th scope="col">Phone</th>

                        <th scope="col">Date</th>





                    </tr>
                    </thead>
                    <tbody>
                    @php

                        $i=1;

                    @endphp
                    @foreach($patient as $patient)

                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                           <td>{{ $patient->name }}</td>
                            <td>{{ $patient->doctor_name }}</td>
                            <td>{{ $patient->phone }}</td>
                            <td>{{ $patient->date }}</td>





                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection

{{--@include('admin.layouts.master')--}}



