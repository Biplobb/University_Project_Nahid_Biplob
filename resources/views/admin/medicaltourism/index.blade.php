{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <h1>HOspital Table</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Hospital</th>
                <th scope="col">Country</th>
                <th scope="col">Branch</th>
                <th scope="col">Address</th>
                <th scope="col">Overview</th>
                <th scope="col">Service</th>
                <th scope="col">procedures</th>
                <th scope="col">photo</th>

                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($Hospital as $hospital_service)
                <tr>
                    <th scope="row">{{ $hospital_service->id }}</th>
                    <td>{{ $hospital_service->hospital }}</td>
                    <td>{{ $hospital_service->country }}</td>
                    <td>{{ $hospital_service->branch }}</td>
                    <td>{{ $hospital_service->address }}</td>

                    <td>{{ $hospital_service->overview }}</td>

                    <td>{{ $hospital_service->service }}</td>
                    <td>{{ $hospital_service->procedures }}</td>
                    <td>{{ $hospital_service->photo }}</td>

                    <td>
                        <a href="{{ route('tourismedit',['id'=>$hospital_service->id]) }}">Edit</a>
                        <a href="{{ route('tourismdelete',['id'=>$hospital_service->id]) }}">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}
