
@extends('admin.layouts.master')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>HOspital </h2>
                <small class="text-muted">HOspital information submit form</small>
            </div>
            <div class="row clearfix">
                {!! Form::open(['url' => '/admin/hospital/update/'.$Hospital->id,'enctype'=>'multipart/form-data','method'=>'patch']) !!}
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>HOspital Information</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder=" Name" name="name" value="{{ $Hospital->name }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>Location</label>
                                            <input type="text" class="form-control" placeholder="location" name="location" value="{{ $Hospital->location }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>Contact</label>
                                            <input type="text" class="form-control" placeholder="location" name="contact" value="{{ $Hospital->contact }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>Review</label>
                                            <input type="text" class="form-control" placeholder="review" name="review" value="{{ $Hospital->review }}">
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>photo update</label>
                                            <input type="file" class="form-control" placeholder="photo update" name="photo" value="{{ $Hospital->photo }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>Service</label>
                                            <select value="{{ $Hospital->service }}" name="service[]" multiple="" class="ui fluid dropdown">
                                                <option value="">service</option>
                                                <option value="operation">operation</option>
                                                <option value="medicine">medicine</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>






                                <div class="col-sm-6 col-xs-12">

                                </div>

                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label>About</label>
                                            <input type="text" class="form-control" placeholder="about" name="about"  value="{{ $Hospital->about }}">
                                        </div>
                                    </div>
                                </div>






                                <div class="col-sm-6 col-xs-12">

                                </div>
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-raised g-bg-cyan">update</button>
                                    <button type="submit" class="btn btn-raised">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>


        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

