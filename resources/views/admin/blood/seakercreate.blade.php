@extends('admin.layouts.master')
@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <p class="text-align:center text-success">{{Session::get('message')}}</p>
            </div>

            <div class="row clearfix">
                {!!   Form::open(['url'=>'admin/blood/seakerstore','method'=>'POST','class'=>'form-horizontal'])!!}
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>Basic Information <small>Description text here...</small> </h2>
                        </div>
                        <div class="body">

                            <div class="row clearfix">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select name="donar" id="" required>
                                                <option value="" checked>Donar</option>
                                                <option value="Panthapath">Panthapath</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" class="form-control" placeholder="Full Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="hname" class="form-control" placeholder="Hospital Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="haddress" class="form-control" placeholder="Hospital Address">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="relationship" class="form-control" placeholder="Relationship">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="">Gender</label>
                                        <input type="radio" id="contactChoice11"
                                               name="gender" value="Male">
                                        <label for="contactChoice11">Male</label>
                                        <input type="radio" id="contactChoice10"
                                               name="gender" value="Female">
                                        <label for="contactChoice10">Female</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="phone" class="form-control" placeholder="Phone Number">
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select name="area" id="" required>
                                                <option value="" checked>Area</option>
                                                <option value="Panthapath">Panthapath</option>
                                                <option value="Dhanmondi32">Dhanmondi 32</option>
                                                <option value="Dhanmondi27">Dhanmondi 27</option>
                                                <option value="ScienceLab">Science Lab</option>
                                                <option value="GreenRoad">Green Road</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                                <button type="submit" class="btn btn-raised">Cancel</button>
                            </div>
                            {!!  Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection