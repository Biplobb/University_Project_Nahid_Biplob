{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>All Donar Request List</h2>
            <small class="text-muted">Welcome to Swift application</small>
        </div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="body">
                <div class="member-card verified">
                    <div class="">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th scope="col">SL</th>
                                <th scope="col">Donor Name</th>
                                <th scope="col">Donor address</th>
                                <th scope="col">Donor phone Number</th>
                                <th scope="col">Last Donate date</th>
                                <th scope="col"> Blood_group</th>
                                <th scope="col">Donor Email</th>
                                <th scope="col">Donor District</th>
                                <th scope="col">Donor Upozila</th>
                                <th scope="col">Request Id</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($seakers as $seakers)
                                <tr>
                                    <th scope="row"> {{ $seakers->id }}</th>
                                    <td>{{ $seakers->name }}</td>
                                    <td>{{$seakers->address }}</td>
                                    <td>{{ $seakers->phone }}</td>
                                    <td>{{ $seakers->date }}</td>
                                    <td>{{ $seakers->blood_group}}</td>
                                    <td>{{ $seakers->email }}</td>
                                    <td>{{ $seakers->area }}</td>
                                    <td>{{ $seakers->upozila }}</td>
                                    <td>{{ $seakers->donor_id }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <button onclick="goBack()">Go Back</button>
                    <script>
                        function goBack() {
                            window.history.back();
                        }
                    </script>

                </div>
            </div>
        </div>
    </div>
</div>
        </div>
    </section>
@endsection


