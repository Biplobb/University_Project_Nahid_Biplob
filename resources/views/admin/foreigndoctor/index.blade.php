{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <h1>Foreign doctor</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">qualification</th>
                <th scope="col">speciality</th>
                <th scope="col">overview</th>


                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($doctor as $doctor)
                <tr>
                    <th scope="row">{{ $doctor->id }}</th>
                    <td>{{ $doctor->name }}</td>
                    <td>{{ $doctor->qualification }}</td>
                    <td>{{ $doctor->speciality }}</td>
                    <td>{{ $doctor->overview }}</td>
                    <td>

                         <a href="{{ route('editforeigndoctorinfo',['id'=>$doctor->id]) }}">Edit</a>
                        <a href="{{ route('deleteforeigndoctor',['id'=>$doctor->id]) }}">DELETE</a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}
