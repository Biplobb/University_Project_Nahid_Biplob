
@extends('admin.layouts.master')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Add Doctor</h2>
            <small class="text-muted">Doctors information submit form</small>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Profile Information</h2>
                    </div>
                    <div class="body">

                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $er)
                                    {{$er}}
                                @endforeach
                            </div>
                        @endif

                    {!! Form::open(['route' => 'addnewforeigndoctor','enctype'=>'multipart/form-data','method'=>'post','name'=>'addnewforeigndoctor']) !!}
                    {{csrf_field()}}
                        <div class="row clearfix">
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Doctor Name" name="name" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="qualification Name" name="qualification" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="speciality Name" name="speciality" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="overview Name" name="overview" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="email" name="email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="password" name="password" required>
                                    </div>
                                </div>
                            </div>





                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label>upload photo</label>
                                        <input type="file" class="form-control" placeholder="Upload photo" name="photo" required>
                                    </div>
                                </div>
                            </div>



                        </div>


                        <div class="row clearfix">




                          <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="south">Hospitals :</label>
                                        @foreach($data as $d)
                                            <input type="checkbox" name="hospital[]" id="{{$d->id}}" value={{$d->id}}>
                                            <label for="{{$d->id}}">{{$d->hospital}}</label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>


                        </div>




                        <div class="row clearfix">
                            <div class="col-xs-12 ">
                                <button type="submit" class="btn btn-raised g-bg-cyan pull-right">Submit</button>
                                <button type="submit" class="btn btn-raised pull-right">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>


    </div>
</section>
@endsection

{{--@include('admin.layouts.footer')--}}

