{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')

@section('content')

    <section class="content" xmlns="http://www.w3.org/1999/html">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>Add Chmaber Schedule for <span style="color: #00b6f1">{{$doctors->name}}</span></h2>--}}
                <small class="text-muted">Add or Remove Doctor Chamber/Hospital</small>
            </div>

            @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
            @endif
            @if(session('exist'))
                <div class="alert alert-danger">
                    {{session('exist')}}
                </div>
            @endif

            <form action="{{route('submitRole')}}" method="post" name="scheduleform" id="myform">
                {{csrf_field()}}
                <div class="row clearfix">
                    <div class="col-md-12 col-xs-12">
                        <div class="card">

                            <div class="header">
                                <h2>Add or Remove Doctor Schedule<small>Select options bellow ...</small> </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            <label for="doctor_id">Select Doctor</label>
                                            <span id="validatedoctorid" class="font-bold col-pink"></span>
                                            <select class="form-control show-tick" name="admin_id" required>
                                                <option value="">Select admin</option>
                                                    <option value="{{$userInfo->id}}">{{$userInfo->name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group drop-custum">
                                            <label for="doctor_id">Select Hospital</label>
                                            <span id="validatehospital_id" class="font-bold col-pink"></span>
                                            <select class="form-control show-tick" name="role_id" required>
                                                <option value="">Select role</option>
                                                @foreach($roles as $r)
                                                    <option value="{{$r->id}}">{{$r->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group ">
                                            <button type="submit" name="submitbutton" class="btn  btn-raised btn-success waves-effect" id="confirm" value="add">Add role</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </section>

@endsection


@section('script')

@endsection