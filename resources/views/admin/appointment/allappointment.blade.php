
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <div class="container-fluid">

            <div class="block-header">
                <h2>All Appointments records</h2>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="member-card verified">
                                <ul class="header-dropdown">
                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="zmdi zmdi-more-vert"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Edit</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Delete</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Block</a></li>
                                        </ul>
                                    </li>
                                </ul>

                                <div class="table">

                                    <table class="table table-responsive" id="apptdatatable">
                                        <thead>
                                        <tr>
                                            <th scope="col">Sl.</th>
                                            <th scope="col">Patient name</th>
                                            <th scope="col">Phone no.</th>
                                            <th scope="col">Address</th>
                                            <th scope="col">Doctor name</th>
                                            <th scope="col">Hospital name</th>
                                            <th scope="col">Day</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Time</th>
                                            <th scope="col">Confirm</th>
                                            <th scope="col">Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i=1;
                                        @endphp
                                        @foreach($data as $d)
                                            <tr>
                                                <td >{{$i++}}</td>
                                                <td >{{$d->name}}</td>
                                                <td >{{$d->phone}}</td>
                                                <td >{{$d->address}}</td>
                                                <td>{{ $d->doctor_name}}</td>
                                                <td>{{ $d->hospital_name}}</td>
                                                <td>{{ $d->day}}</td>
                                                <td>{{ $d->date}}</td>
                                                <td>{{ $d->time}}</td>
                                                <td>
                                                    <button type="button" class="btn btn-raised  btn-danger waves-effect" id="confirm" data-id="{{$d->apptid}}" data-toggle="modal" data-target="#defaultModal"> Confirm </button>
                                                </td>
                                                <td><button type="button" class="btn btn-raised  btn-success waves-effect" id="delete" data-id="{{$d->apptid}}" data-toggle="modal" data-target="#deletemodal"> Delete </button></td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section>


    <div class="modal fade Modal-Col-Pink"  id="defaultModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body"> <strong><p>Do you want to confirm this appointment? </p></strong> </div>
                <div class="modal-footer">
                    <input type="hidden" name="apptid" id="apptid">
                    <button type="button" id="close" class="btn btn-raised  btn-success waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="button" id="yesconfirm" class="btn btn-raised  btn-danger waves-effect" data-dismiss="modal">CONFIRM</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade Modal-Col-Pink"  id="deletemodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body"> <strong><p>Do you want to confirm this appointment? </p></strong> </div>
                <div class="modal-footer">
                    <input type="hidden" name="apptiddelete" id="apptiddelete">
                    <button type="button" id="close" class="btn btn-raised  btn-success waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="button" id="yesdelete" class="btn btn-raised  btn-danger waves-effect" data-dismiss="modal">DELETE</button>
                </div>
            </div>
        </div>
    </div>
@endsection

{{--@include('admin.layouts.footer')--}}

@section('script')
    {{--<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>--}}
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#apptdatatable').DataTable();
        });

        $(document).on('click','#confirm',function () {
            $('#apptid').val($(this).data('id'));
        });

        $(document).on('click','#delete',function () {
            $('#apptiddelete').val($(this).data('id'));
        });

        $(document).on('click','#yesconfirm',function () {
            var appptid=$('#apptid').val();
            $.ajax({
                type:'post',
                url:'{{route('confirmAppointment')}}',
                data:{
                    _token:'{{csrf_token()}}',
                    apptid:appptid
                },
                success:function (data) {
                    $('#apptdatatable').load(location.href + ' #apptdatatable');
                }
            });
        });
        $(document).on('click','#yesdelete',function () {
            var appptid=$('#apptiddelete').val();
            $.ajax({
                type:'post',
                url:'{{route('deleteAppointment')}}',
                data:{
                    _token:'{{csrf_token()}}',
                    apptid:appptid
                },
                success:function (data) {

                    $('#apptdatatable').load(location.href + ' #apptdatatable');
                }
            });
        });
    </script>
@endsection
