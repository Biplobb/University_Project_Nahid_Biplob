@extends('admin.layouts.master')
@section('content')

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>Add Blog</h2>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        {!! Form::open(['route' => array('updateblog', $blog->id),'enctype'=>'multipart/form-data','method'=>'patch']) !!}

                        <input type="hidden" class="form-control" placeholder="Doctor Name"  value="{{ $blog->doctor_id }}"  name="title">
                        <div class="body">
                            <div class="row clearfix">


                                <div class="body">
                            <div class="row clearfix">

                                <div class="body">
                            <div class="row clearfix">

                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Post Title"  value="{{ $blog->title }}"  name="title">
                                        </div>
                                    </div>
                                </div>



                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Author" value="{{ $blog->author }}"  name="author">
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-sm-2">Type</div>
                                <div class="col-lg-3">


                                    <select name="type" class="form-control">
                                        <option value="1" @php if($blog->type=='1')
                                        {
                                        echo 'selected';
                                        }@endphp>Featured</option>
                                        <option value="0" @php if($blog->type=='0')
                                        {echo 'selected';}@endphp>Regular</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        {{ Form::textarea('blog_post', $blog->blog_post)}}
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-12">
                            <div class="form-group">

{{--                                @if ("{{asset('storage/image/'.$blog->image)}}")--}}
                                    <img src="{{asset('image/blog-photo/'.$blog->image)}}" width="400px",height="400px">
                                {{--@else--}}
                                    {{--<p>No Image Found</p>--}}
                                {{--@endif--}}


                                {{--image <input type="file" name="image" value="{{ $blog->image }}"/>--}}
                            </div>

                                <div class="form-control">

                                    <label for="Image">Update Image</label>
                                    <input type="file" name="image">

                                </div>
                                <br><br>
                            </div>





                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-raised g-bg-cyan"   >update</button>
                                <button type="submit" class="btn btn-raised">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {!! Form::close() !!}
    </section>
@endsection

{{--@include('admin.layouts.footer')--}}
