@extends('../layouts.master')
@section('title', 'Deshidoctor | Join')
@section('content')
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 bread-block text-center">
                    <h2>Join as a diagnostic</h2>
                    <h3 style="color:red;text-align: center">
                        <?php
                        $exception = Session::get('exception');
                        if($exception){
                            echo $exception;
                            Session::put('exception',null);
                        }
                        ?>
                    </h3>
                    <h3 style="color:green;text-align: center">
                        <?php
                        $message = Session::get('message');
                        if($message){
                            echo $message;
                            Session::put('message',null);
                        }
                        ?>
                    </h3>
                </div>
            </div>
        </div>
    </section>
    <section id="about-us" class="space-top bg-color v2">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 no-padding">

                    @foreach ($errors->all() as $message)
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @endforeach

                    @if(session('success'))
                        <div class="alert alert-success">
                            {{session('success')}}
                        </div>
                    @endif
                    {!! Form::open(['route' => 'storediagnostic','enctype'=>'multipart/form-data','method'=>'post']) !!}
                    {{csrf_field()}}
                    <div class="col-sm-12">
                        <div class="form-group col-sm-6">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" id="email" placeholder="Your email" required>
                        </div>


                        <div class="form-group col-sm-6">
                            <label for="email1">Password</label>
                            <input type="password" class="form-control" name="password" id="email1" placeholder="password" required>
                        </div>



                        <div class="form-group col-sm-6">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control"  name="phone" id="phone" required>
                        </div>







                        <div class="form-group col-sm-6">
                            <label for="location">Address/Location</label>
                            <input type="text" class="form-control"  name="location" id="location" required>
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="About">About</label>
                            <input type="text" class="form-control"  name="about" id="About" required>
                        </div>









                        <div class="form-group col-sm-6">
                            <label for="image">Your Image</label>
                            <input type="file" class="form-control"  name="photo" id="image" required>
                        </div>
                    </div>


                    <div class="form-group col-sm-12">
                        <h4>Terms and conditions</h4>
                        <p> All time our service will be first</p>
                    </div>


                    <div class="form-group col-sm-12">
                        <div class="button-item pull-right">
                            <label class="checkbox-inline"><input type="checkbox" id="termsaccept" name="termsaccept" value="">I accept the term and conditions.</label>
                            <button class="btn radius-4x" id="submitbtn">Submit</button>
                        </div>
                    </div>

                    </form>
                </div>
                <div class="col-sm-4 col-sm-offset-1 about-block text-right">
                    <img src="{{ asset('front-end/assets/images/diagnostic.jpg') }}" alt="Columba">
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#submitbtn").attr("disabled", "disabled");
        });

        $(document).on('click','#termsaccept',function () {
            if ($('#termsaccept').is(':checked')) {
                $('#termsaccept').val("yes");
                $("#submitbtn").attr("disabled", false);
            }else{
                $('#termsaccept').val("not");
                $("#submitbtn").attr("disabled", true);
            }
        })
    </script>

@endsection