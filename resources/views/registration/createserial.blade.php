@extends('../layouts.master')
@section('title', 'Deshidoctor | Join')
@section('content')
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 bread-block text-center">
                    <h2>SERIAL ADD</h2>
                    <h3 style="color:red;text-align: center">
                        <?php
                        $exception = Session::get('exception');
                        if($exception){
                            echo $exception;
                            Session::put('exception',null);
                        }
                        ?>
                    </h3>
                    <h3 style="color:green;text-align: center">
                        <?php
                        $message = Session::get('message');
                        if($message){
                            echo $message;
                            Session::put('message',null);
                        }
                        ?>
                    </h3>
                </div>
            </div>
        </div>
    </section>
    <section id="about-us" class="space-top bg-color v2">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 no-padding">

                    @foreach ($errors->all() as $message)
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @endforeach

                    @if(session('success'))
                        <div class="alert alert-success">
                            {{session('success')}}
                        </div>
                    @endif

                    {!! Form::open(['route' => 'storehospitalserial','enctype'=>'multipart/form-data','method'=>'post']) !!}
                    {{csrf_field()}}

                    <div class="col-sm-12">

                       {{-- @foreach($doctor as $doctor)--}}
                        <div class="form-group col-sm-6">
                            <label for="name">Hospital name</label>

                            <input type="text" class="form-control" value="{{ $doctor[0]->hospitalname }} "   id="name" placeholder="hospital name" required>
                            <input type="hidden" class="form-control" value="{{ $doctor[0]->hospitalid }} "   name="hospital_id" id="name" placeholder="hospital name" required>

                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">Date</label>
                            <input type="date" class="form-control" name="date" id="email" placeholder="Date" required>
                        </div>


                        <div class="form-group col-sm-6">
                            <label for="email1">Patient Name</label>
                            <input type="text" class="form-control" name="name" id="email1" placeholder="Patient Name" required>
                        </div>



                        <div class="form-group col-sm-6">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control"  name="phone" id="phone" required>
                        </div>


                        <div class="form-group col-sm-6">
                            <label for="phone">doctor_name</label>


                            <select class="form-control " name="doctor_name" required>

                                @for($i=0;$i<$a;$i++)

                                    <option value="{{$doctor[$i]->doctorname}}">{{$doctor[$i]->doctorname}}</option>
                                @endfor

                            </select>


                        </div>










                        <div class="form-group col-sm-12">
                        <h4>Terms and conditions</h4>
                        <p> All time our service will be first</p>
                    </div>


                    <div class="form-group col-sm-12">
                        <div class="button-item pull-right">
                            <label class="checkbox-inline"><input type="checkbox" id="termsaccept" name="termsaccept" value="">I accept the term and conditions.</label>
                            <button class="btn radius-4x" id="submitbtn">Submit</button>
                        </div>
                    </div>

                    </form>
                           {{-- @endforeach--}}
                </div>

            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#submitbtn").attr("disabled", "disabled");
        });

        $(document).on('click','#termsaccept',function () {
            if ($('#termsaccept').is(':checked')) {
                $('#termsaccept').val("yes");
                $("#submitbtn").attr("disabled", false);
            }else{
                $('#termsaccept').val("not");
                $("#submitbtn").attr("disabled", true);
            }
        })
    </script>

@endsection