
@extends('registration.layouts.master')
{{--@include('admin.layouts.header')--}}
@section('content')

    <section class="content home">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Dashboard</h2>
                <small class="text-muted">Welcome to Swift application</small>
            </div>

            <div class="">

                <table class="table">
                    <thead>


                    <tr>
                        <th scope="col">Sl.</th>
                        <th scope="col">Patient Name</th>
                        <th scope="col">Doctor Name</th>
                        <th scope="col">phone</th>
                        <th scope="col">Date</th>
                        <th scope="col">Hospital Name</th>




                    </tr>
                    </thead>
                    <tbody>
                    @php

                        $i=1;

                    @endphp
                    @foreach($doctor as $doctorprofile)

                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $doctorprofile->serialname }}</td>
                            <td>{{ $doctorprofile->serialdoctor_name }}</td>
                            <td>{{ $doctorprofile->serialphone }}</td>
                            <td>{{ $doctorprofile->serialdate }}</td>
                            <td>{{ $doctorprofile->hospitalname }}</td>



                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection

{{--@include('admin.layouts.master')--}}



