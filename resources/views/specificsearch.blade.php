@extends('layouts.master')
@section('title', 'Deshidoctor | Search')
@section('content')

    <section id="blog" class=" bg-color v2">
        <div class="container">
            <div class="row">
                {{--<div class="col-sm-9 col-sm-offset-3 search-page">--}}
                    {{--<div class="search-box text-center">--}}
                        {{--<form>--}}
                            {{--<select>--}}
                                {{--<option>Dhaka</option>--}}
                                {{--<option>Gazipur</option>--}}
                                {{--<option>Rangpur</option>--}}
                                {{--<option>Rajshahi</option>--}}
                            {{--</select>--}}
                            {{--<select>--}}
                                {{--<option>Dhaka</option>--}}
                                {{--<option>Gazipur</option>--}}
                                {{--<option>Rangpur</option>--}}
                                {{--<option>Rajshahi</option>--}}
                            {{--</select>--}}
                            {{--<input type="text" name="">--}}
                            {{--<button type="submit" name=""><i class="fa fa-search"></i></button>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="row">
                {{--<aside class="col-sm-4 col-md-3">--}}
                    {{--<div class="widget category animate-in move-up animated">--}}
                        {{--<h5>Categories</h5>--}}
                        {{--<ul>--}}
                            {{--<li><a href="#">Photoshop<span>(5)</span></a> </li>--}}
                            {{--<li><a href="#">Photography<span>(5)</span></a> </li>--}}
                            {{--<li><a href="#">Design<span>(5)</span></a> </li>--}}
                            {{--<li><a href="#">Development<span>(5)</span></a> </li>--}}
                            {{--<li><a href="#">Illustrator<span>(5)</span></a> </li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                    {{--<div class="widget recent-post animate-in move-up animated">--}}
                        {{--<h5>Recent Posts</h5>--}}
                        {{--<ul>--}}
                            {{--<li><a href="#">Smarter Grids With Sass And ...</a> </li>--}}
                            {{--<li><a href="#">Quantity Ordering With CSS</a> </li>--}}
                            {{--<li><a href="#">Gallery Post</a> </li>--}}
                            {{--<li><a href="#">Video Post</a> </li>--}}
                            {{--<li><a href="#">Image Post</a> </li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</aside>--}}
                <div class="col-md-12">
{{--                    @foreach($data as $d)--}}
                    <article class="col-sm-12 blog-block animate-in move-up animated v1">
                        <div class="blog-heading choose-features">
                            <div class="choose-item no-padding">
                                <div class="icon" style="display: inline-block">
                                    <img src="{{asset('image/doctor-photo/'.$data->photo)}}" alt="Columba" class="img-circle" height="200px" width="200px">
                                </div>
                                <div class="choose-text">
                                    <h4>
                                        <a href="#">{{$data->name}}</a>
                                    </h4>
                                    <p>MBBS, MCPS ( Med. ), D-Card
                                        Professor & Head, Dept. of Cardiology, Northern International Medical College Hospital</p>
                                </div>
                            </div>

                        </div>
                        <p> <i class="fa fa-map-marker"></i> Chamber  <br />
                            Northern International Medical College Hospital, Main Branch, Dhanmondi, Dhaka
                            <br />
                            <i class="fa fa-money"></i> New appointment: 500 TK.</p>

                        <a href="{{route('doctor_profile',['id'=>$data->id])}}" class="btn green radius-2x">Book Appointment</a>
                    </article>
                    {{--@endforeach--}}
                    {{--<article class="col-sm-6 blog-block animate-in move-up animated v1">--}}
                        {{--<div class="blog-heading choose-features">--}}
                            {{--<div class="choose-item no-padding">--}}
                                {{--<div class="icon" style="display: inline-block">--}}
                                    {{--<img src="http://localhost:8000/front-end/assets/images/doctor2.jpg" alt="Columba" class="img-circle">--}}
                                {{--</div>--}}
                                {{--<div class="choose-text">--}}
                                    {{--<h4>--}}
                                        {{--<a href="#">Prof. Dr. Nurul Alam Talukdar</a>--}}
                                    {{--</h4>--}}
                                    {{--<p>MBBS, MCPS ( Med. ), D-Card--}}

                                        {{--Professor & Head, Dept. of Cardiology, Northern International Medical College Hospital</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<p> <i class="fa fa-map-marker"></i> Chamber  <br />--}}
                            {{--Northern International Medical College Hospital, Main Branch, Dhanmondi, Dhaka--}}
                            {{--<br />--}}
                            {{--<i class="fa fa-money"></i> New appointment: 500 TK.</p>--}}

                        {{--<a href="#" class="btn green radius-2x">View Profile</a>--}}
                    {{--</article>--}}
                    {{--<article class="col-sm-6 blog-block animate-in move-up animated v1">--}}
                        {{--<div class="blog-heading choose-features">--}}
                            {{--<div class="choose-item no-padding">--}}
                                {{--<div class="icon" style="display: inline-block">--}}
                                    {{--<img src="http://localhost:8000/front-end/assets/images/doctor3.jpg" alt="Columba" class="img-circle">--}}
                                {{--</div>--}}
                                {{--<div class="choose-text">--}}
                                    {{--<h4>--}}
                                        {{--<a href="#">Prof. Dr. Nurul Alam Talukdar</a>--}}
                                    {{--</h4>--}}
                                    {{--<p>MBBS, MCPS ( Med. ), D-Card--}}

                                        {{--Professor & Head, Dept. of Cardiology, Northern International Medical College Hospital</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<p> <i class="fa fa-map-marker"></i> Chamber  <br />--}}
                            {{--Northern International Medical College Hospital, Main Branch, Dhanmondi, Dhaka--}}
                            {{--<br />--}}
                            {{--<i class="fa fa-money"></i> New appointment: 500 TK.</p>--}}

                        {{--<a href="#" class="radius-2x btn green">View Profile</a>--}}
                    {{--</article>--}}
                    {{--<article class="col-sm-6 blog-block animate-in move-up animated v1">--}}
                        {{--<div class="blog-heading choose-features">--}}
                            {{--<div class="choose-item no-padding">--}}
                                {{--<div class="icon" style="display: inline-block">--}}
                                    {{--<img src="http://localhost:8000/front-end/assets/images/doctor4.jpg" alt="Columba" class="img-circle">--}}
                                {{--</div>--}}
                                {{--<div class="choose-text">--}}
                                    {{--<h4>--}}
                                        {{--<a href="#">Prof. Dr. Nurul Alam Talukdar</a>--}}
                                    {{--</h4>--}}
                                    {{--<p>MBBS, MCPS ( Med. ), D-Card--}}

                                        {{--Professor & Head, Dept. of Cardiology, Northern International Medical College Hospital</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<p> <i class="fa fa-map-marker"></i> Chamber  <br />--}}
                            {{--Northern International Medical College Hospital, Main Branch, Dhanmondi, Dhaka--}}
                            {{--<br />--}}
                            {{--<i class="fa fa-money"></i> New appointment: 500 TK.</p>--}}

                        {{--<a href="#" class="radius-2x btn green">View Profile</a>--}}
                    {{--</article>--}}


                        {{--<a href="#" class="radius-2x btn green">View Profile</a>--}}
                    {{--</article>--}}

                {{--</div>--}}

            </div>
            </div>
        </div>
    </section>
@endsection