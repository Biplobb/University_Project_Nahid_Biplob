
@extends('search.search.layouts.master')




@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Patient all information</h2>
                <small class="text-muted">Patient information  </small>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Profile Information</h2>
                        </div>
                        <div class="body">

                            @if($errors->any())
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $er)
                                        {{$er}}
                                    @endforeach
                                </div>
                            @endif


                            @foreach($patient as $patient)
                                <div class="row clearfix">

                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient Name:</h3>
                                                <h4>{{$patient->name}}</h4>

                                            </div>
                                        </div>
                                    </div>







                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient Present Address:</h3>
                                                <h4>{{$patient->address}}</h4>
                                            </div>
                                        </div>
                                    </div>

                                </div>



                                <div class="row clearfix">

                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient contact number:</h3>
                                                <h4>{{$patient->phone}}</h4>

                                            </div>
                                        </div>
                                    </div>







                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient occupation :</h3>
                                                <h4>{{$patient->occupation}}</h4>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row clearfix">


                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient Image:</h3>
                                                <img height="200" width="150" src="{{ asset('image/hospital-photo/'.$patient->photo)}}" alt="">


                                            </div>
                                        </div>
                                    </div>
                                </div>




                        </div>


                            {!! Form::open(['route' => 'storereport','enctype'=>'multipart/form-data','method'=>'post']) !!}
                            {{csrf_field()}}
                            <div class="col-sm-12">
                                <div class="form-group col-sm-6">
                                    <h3>Ptient Name</h3>

                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                                </div>
                                <div class="form-group col-sm-6">
                                    <h3>Patient Id</h3>

                                    <input type="text" class="form-control" name="id" id="name" placeholder="Patient id" required>
                                    <input type="hidden" value="{{$patient->id}}" class="form-control" name="patient_id" >
                                </div>



                                <div class="form-group col-sm-6">
                                    <h3>Patient report(must be pdf)</h3>

                                    Please click!<input type="file" class="form-control"  name="photo" id="image" required>
                                </div>
                            </div>


                            <div class="form-group col-sm-12">
                                <h4>Terms and conditions</h4>
                                <p> If any mistake pdf file, all risk your</p>
                            </div>


                            <div class="form-group col-sm-6">
                                <div class="button-item pull-right">

                                    <button class="btn radius-4x" id="submitbtn">Submit</button>
                                </div>
                            </div>

                            </form>


                        @endforeach








                    </div>




                </div>
            </div>
        </div>

        </div>


        </div>
    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

