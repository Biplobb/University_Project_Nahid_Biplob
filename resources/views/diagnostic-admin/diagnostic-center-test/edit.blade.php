
@extends('diagnostic-admin.layouts.master')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Add Diagnostic Center Tests </h2>
                <small class="text-muted">Diagnostic Center Tests information submit form</small>
            </div>
            <div class="row clearfix">
                {!! Form::open(['url' => '/admin/diagnostic/diagnostic-center-test/update/'.$diagnostic_center_tests->id,'enctype'=>'multipart/form-data','method'=>'patch']) !!}
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Diagnostic Center Tests Information</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Test Name" name="Tests_id" value="{{ $diagnostic_center_tests->Tests_id }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="previous price" name="previous" value="{{ $diagnostic_center_tests->previous }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="previous price" name="discountprice" value="{{ $diagnostic_center_tests->discountprice }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Discount price" name="discount" value="{{ $diagnostic_center_tests->discount }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Price" name="Diagnostic_Center_id"  value="{{ $diagnostic_center_tests->Diagnostic_Center_id }}">
                                        </div>
                                    </div>
                                </div>






                                <div class="col-sm-6 col-xs-12">

                                </div>
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-raised g-bg-cyan">update</button>
                                    <button type="submit" class="btn btn-raised">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>


        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

