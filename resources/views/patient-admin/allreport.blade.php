
@extends('patient-admin.layouts.master1')


{{--@include('admin.layouts.header')--}}
@section('content')

    <section class="content home">

        <div class="container-fluid">
            <div class="block-header">
                {{-- <div class="titless" style="overflow:hidden">
                     <div class="title" align="center"  >
                         {!! Form::open(['route' => 'searchPDFdoctor','method'=>'post','class'=>'class_name']) !!}
                         {!! Form::text('title', null, array('placeholder' => '        Search Text','id'=>'search_text')) !!}

                         <button class="clickable" id="clickable">.        search    .</button>

                         --}}{{--{!! Form::submit('search') !!}--}}{{--
                         {!! Form::close() !!}
                     </div>
                 </div>--}}
                <h2>Dashboard</h2>
                <small class="text-muted">Welcome to Swift application</small>

            </div>


            <div class="">

                <table class="table">
                    <thead>


                    <tr>
                        <th scope="col">Sl.</th>
                        <th scope="col">Report Title</th>
                        <th scope="col">Report File</th>

                        <th scope="col">Action</th>




                    </tr>
                    </thead>
                    <tbody>
                    @php

                        $i=1;

                    @endphp
                    @foreach($report as $report)

                        <tr>
                            <th scope="row">{{ $i++ }}</th>
                            <td>{{ $report->name }}</td>
                            <td>{{ $report->photo }}</td>
                            <td>
                                <a href="{{ asset('image/hospital-photo/'.$report->photo) }}" download><img height="50" width="50" src="{{asset('image/hospital-photo/pdf-icon.jpg')}}"/></a>

                                {{--<a id="del" href="">Delete</a>--}}
                            </td>




                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection

{{--@include('admin.layouts.master')--}}



