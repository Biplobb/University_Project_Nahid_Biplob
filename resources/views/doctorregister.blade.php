@extends('layouts.master')
@section('title', 'Deshidoctor | Join')
@section('content')
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 bread-block text-center">
                    <h2>Join as a doctor</h2>
                    <p>Welcome massage goes to here</p>
                </div>
            </div>
        </div>
    </section>
    <section id="about-us" class="space-top bg-color v2">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 no-padding">

                    @foreach ($errors->all() as $message)
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @endforeach

                    @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                    @endif
                    <form action="{{route('join')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="col-sm-12">
                            <div class="form-group col-sm-6">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" id="email" placeholder="Your email" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="designation">Title/Designation</label>
                                <input type="text" class="form-control" name="designation" id="designation" placeholder="Title/Designation" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="specialty">Speciality</label>
                                <select class="form-control " name="specialty" required>
                                    <option value="Immunology">Allergy &amp; Immunology</option>
                                    <option value="Anesthesia">Anesthesia</option>
                                    <option value="Cardiology">Cardiology (Heart, Cardiac Surgery, Cardiovascular, Hypertension, Blood Pressure)</option>
                                    <option value="Colorectal Surgery">Colorectal Surgery (Surgery of Anal Canal, Rectum, etc.)</option>
                                    <option value="Dental">Dentistry (Dental, Orthodontics, Maxillofacial Surgery, Scaling)</option>
                                    <option value="Dermatology">Dermatology (Skin, Venereology, Sexual, Hair, Allergy)</option>
                                    <option value="Endocrinology">Endocrinology (Diabetes, Hormones, Thyroid, etc.)</option>
                                    <option value="Otorhinolaryngology">ENT (Ear, Nose &amp; Throat, Otorhinolaryngology)</option>
                                    <option value="Gastroenterology">Gastroenterology (Stomach, Pancreas and Intestine)</option>
                                    <option value="General Physician">General Physician (All or any common diseases and health issues)</option>
                                    <option value="General Surgery ">General Surgery (Incision, Operation)</option>
                                    <option value="Gynaecologic Oncology ">Gynaecologic Oncology (Cancer of Female Reproductive System)</option>
                                    <option value="Gynaecology and Obstetrics">Gynaecology and Obstetrics (Pregnancy, Menstrual, Uterus, Female)</option>
                                    <option value="Haematology">Haematology (Blood related diseases)</option>
                                    <option value="Hepatology">Hepatology (Liver, Gallbladder, Biliary system)</option>
                                    <option value="Infectious Diseases">Infectious Diseases</option><option value="45">Infertility</option>
                                    <option value="Medicine">Medicine (All Diseases of Adults)</option><option value="15">Neonatology (New Born Issues)</option>
                                    <option value="Nephrology">Nephrology (Kidney, Ureter, Urinary Bladder)</option>
                                    <option value="Neuromedicine">Neuromedicine (Brain, Spinal Cord, Nerve)</option>
                                    <option value="Neurosurgery">Neurosurgery (Surgery of Brain, Spinal Cord and Nerve)</option>
                                    <option value="Nutritionist">Nutritionist (Food, Diet, Weight Management)</option>
                                    <option value="Oncology">Oncology (Cancer)</option>
                                    <option value="Ophthalmology">Ophthalmology (Eye, Vision, Cataracts, etc.)</option>
                                    <option value="Orthopaedics">Orthopaedics (Bone, Joint, Fractures)</option>
                                    <option value="Paediatric Surgery">Paediatric Surgery (Surgery for Children)</option>
                                    <option value="Paediatrics">Paediatrics (Child or Infant any disease)</option>
                                    <option value="Physical Medicine">Physical Medicine (Paralysis, Pain Management)</option>
                                    <option value="Physiotherapy">Physiotherapy</option>
                                    <option value="Plastic Surgery, Reconstruction or Cosmetic Surgery">Plastic Surgery, Reconstruction or Cosmetic Surgery</option>
                                    <option value="Psychiatry">Psychiatry (Mental Health, Drug Abuse, Depression, etc.)</option>
                                    <option value="Respiratory Medicine">Respiratory Medicine(Pulmonary, Lung Diseases, Bronchitis etc.</option>
                                    <option value="Rheumatology">Rheumatology (Arthritis, Joint, Soft Tissue problems)</option>
                                    <option value="Sex Specialist">Sex Specialist(Venereology)</option>
                                    <option value="Speech and Language Therapy">Speech and Language Therapy</option>
                                    <option value="Thoracic Surgery ">Thoracic Surgery (Surgery in Chest, Lung, etc.)</option>
                                    <option value="Urology">Urology (Surgery for Kidney, Ureter or Urinary Bladder)</option>
                                    <option value="Vascular Surgery">Vascular Surgery (e.g. Surgery of Blood Vessels)</option></select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="gender">Gender</label>
                                <select class="form-control" name="gender" required>
                                    <option value="m">Male</option>
                                    <option value="f">Female</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control"  name="phone" id="phone" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="location">Address/Location</label>
                                <input type="text" class="form-control"  name="location" id="location" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="reg_no">BMDC Reg. No</label>
                                <input type="text" class="form-control"  name="bmdc_reg_no" id="reg_no" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="nid">National ID No.</label>
                                <input type="text" class="form-control"  name="nid" id="nid" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="reg_no">Degrees/Qualification</label>
                                <input type="text" class="form-control"  name="degree" id="degree">
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="experience">Years of experience</label>
                                <input type="text" class="form-control"  name="experience" id="experience" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="interested-at">Interested at: </label>
                                <label class="checkbox-inline"><input type="checkbox" name="interestedat[]" value="Interview">Interview</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interestedat[]" value="Tele-treatment">Tele-treatment</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interestedat[]" value="Online-appointment">Online appointment</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interestedat[]" value="Video-conference-treatment">Video conference treatment</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interestedat[]" value="Writing-at-doctor-blog">Writing at doctor blog</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interestedat[]" value="Corporate-patient-treatment">Corporate patient treatment</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interestedat[]" value="Medicine-information">Medicine information</label>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="image">Your Image</label>
                                <input type="file" class="form-control"  name="image" id="image" required>
                            </div>
                        </div>


                        <div class="form-group col-sm-12">
                            <h4>Terms and conditions</h4>
                           <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur delectus,
                               dolore dolores facere laboriosam laudantium maiores
                               possimus reiciendis sint soluta tenetur vero. Aperiam debitis hic illum laboriosam maiores minus rem!</p>
                        </div>


                        <div class="form-group col-sm-12">
                            <div class="button-item pull-right">
                                <label class="checkbox-inline"><input type="checkbox" id="termsaccept" name="termsaccept" value="">I accept the term and conditions.</label>
                               <button class="btn radius-4x" id="submitbtn">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-sm-4 col-sm-offset-1 about-block text-right">
                    <img src="{{ asset('front-end/assets/images/why.png') }}" alt="Columba">
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#submitbtn").attr("disabled", "disabled");
        });

        $(document).on('click','#termsaccept',function () {
            if ($('#termsaccept').is(':checked')) {
                $('#termsaccept').val("yes");
                $("#submitbtn").attr("disabled", false);
            }else{
                $('#termsaccept').val("not");
                $("#submitbtn").attr("disabled", true);
            }
        })
    </script>

@endsection