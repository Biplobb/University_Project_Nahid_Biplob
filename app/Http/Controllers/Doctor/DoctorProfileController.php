<?php

namespace App\Http\Controllers\Doctor;

use App\DoctorProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DoctorProfileController extends Controller
{

    public function getDoctorProfile($id){
  $ids=$id;
        $doctor1=DB::table('doctor_hospitals')->select('doctor_hospitals.hospital_id as hospital_id','doctor_hospitals.doctor_id as doctor_id','doctor_profiles.name as doctorname','doctor_profiles.id as doctorid','hospital.name as hospitalname','hospital.id as hospitalid')
            ->leftJoin('doctor_profiles','doctor_profiles.id','=','doctor_hospitals.doctor_id')
            ->leftJoin('hospital','hospital.id','=','doctor_hospitals.hospital_id')
            ->where('doctor_profiles.id','=',$ids)
            ->distinct()
            ->get();


        $a1=sizeof($doctor1);

        $doctor=DB::table('doctor_profiles')->select('name','experience','photo','degree','speciality')->where('id','=',$id)->get()->first();

        $data=DB::table('doctor_hospitals')->select('hospital.name as hospitalname','hospital.id as hospitalid','doctor_profiles.id as doctorid','first_fees','second_fees')
            ->leftJoin('hospital','doctor_hospitals.hospital_id','=','hospital.id')
            ->leftJoin('doctor_profiles','doctor_hospitals.doctor_id','=','doctor_profiles.id')
            ->where('doctor_hospitals.doctor_id','=',$id)
            ->distinct()
            ->get();

        if(sizeof($data)==0){
            return view('404');
        }



        return view('profile',compact('data','doctor','doctor1','a1'));
    }
    public function getschedule(Request $request)
    {
        $option='';
        $data=DB::table('dcotor_schedules')->select('hospital_id','doctor_id','day_id','value','day_name','start','end','interval')
            ->leftJoin('hospital','dcotor_schedules.hospital_id','=','hospital.id')
            ->leftJoin('doctor_profiles','dcotor_schedules.doctor_id','=','doctor_profiles.id')
            ->leftJoin('days','dcotor_schedules.day_id','=','days.id')
            ->where('dcotor_schedules.doctor_id','=',$request->doctor_id)
            ->where('dcotor_schedules.hospital_id','=',$request->hospital_id)
            ->get();


        if(sizeof($data)!=0)
        {
            $option.='<label>Select time: <span style="color: red">*<span> </label>';
            foreach ($data as $d) {
                $interval = date_interval_create_from_date_string($d->interval . ' minutes');
                $begin = date_create($d->start);
                $end = date_create($d->end)->add($interval);
                $option.="<select id='$d->day_name' class='form-control times' style='margin-top: 5px' name='appttime'>";
                $option.="<option value=''>Select time</option>";
                foreach (new \DatePeriod($begin, $interval, $end) as $dt) {
//                    $option.='<a class="btn blue sm-size" id="time" data-time='.$dt->format('h:i:a').' style="margin: 2px">'.$dt->format('h:i:a').'</a>';
                    $option.='<option class="btn blue sm-size" id="time" value='.$dt->format('h:i:a').' style="margin: 2px">'.$dt->format('h:i:a').'</option>';
                };

                $option.="</select>";

            }

        }
        return array($data,$option);
//        return $option;
    }

}
