<?php

namespace App\Http\Controllers\Doctor;

use App\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EachDoctorAppointmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:doctor');
    }

    public function getAllAppointments()
    {
        $data=DB::table('appointments')->select('appointments.id as id','date','time','appointments.name as patientname','appointments.phone as phone','hospital.name as hospital_name','appointments.address')
            ->leftJoin('doctor_profiles','doctor_profiles.id','appointments.doctor_id')
            ->leftJoin('hospital','hospital.id','appointments.hospital_id')
            ->where('status','=','not')
            ->where('deleted_at','=',null)
            ->where('doctor_id','=',Auth::guard('doctor')->user()->id)
            ->get();
        return view('doctor.myappointment',compact('data'));
    }

    public function confirmAppointmentByDoctor(Request $request)
    {
        DB::table('appointments')
            ->where('id', $request->apptid)
            ->update(['status' => 'yes']);
    }

    public function deleteAppointmentByDoctor(Request $request)
    {
        Appointment::where('id', $request->apptid)->delete();
    }
}
