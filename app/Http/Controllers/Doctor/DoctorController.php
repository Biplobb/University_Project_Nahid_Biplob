<?php

namespace App\Http\Controllers\Doctor;

use App\DoctorProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DoctorController extends Controller
{

    public  function __construct()
    {
        $this->middleware('auth:doctor');
    }

    public function index()
    {
        $doctorprofile=DB::table('doctor_profiles')->select('name','id','email','speciality','phone','address','speciality','member','awards','experience','photo')
            ->where('doctor_profiles.id','=',Auth::guard('doctor')->user()->id)->get()->first();
        return view('doctor/dashboard',compact('doctorprofile'));
    }

    public function doctorchamber()
    {
        $data=DB::table('dcotor_schedules')->select('dcotor_schedules.id as scheduleid','hospital.name as chamber','hospital.id as hospitalid','location','start','end','day_name','days.id as day_id','available')
            ->leftJoin('doctor_profiles','doctor_profiles.id','dcotor_schedules.doctor_id')
            ->leftJoin('hospital','hospital.id','dcotor_schedules.hospital_id')
            ->leftJoin('days','days.id','dcotor_schedules.day_id')
            ->where('doctor_profiles.id','=',Auth::guard('doctor')->user()->id)->get();
        return view('doctor.doctorchamber',compact('data'));
    }



    public function updateschedulebydoctor(Request $request)
    {
        DB::table('dcotor_schedules')->where('id','=',$request->scheduleid)
            ->update(['start'=>$request->start,'end'=>$request->end,'day_id'=>$request->day,'available'=>$request->available]);
        return redirect()->back()->with('updated','Schedule updated');
    }

    public function update(Request $request)
    {
        DB::table('doctor_profiles')->where('id','=',Auth::guard('doctor')->user()->id)
            ->update(['name'=>$request->name,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'address'=>$request->address,
                'member'=>$request->member,
                'awards'=>$request->awards,
                'experience'=>$request->experience,
            ]);
        return redirect()->back()->with('updated','Profile updated');
    }


}
