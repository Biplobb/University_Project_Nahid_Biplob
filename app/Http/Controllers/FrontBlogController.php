<?php

namespace App\Http\Controllers;
use App\blog;
use App\DoctorProfile;
use App\comment;
use Illuminate\Http\Request;


use App\Hospital;

use Carbon\Carbon;
use DB;
use Session;
use Redirect;
use Mail;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class FrontBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog=DB::table('blogs')->select('*')->get();
        $categories=DB::table('categories')->select('*')->get();

        $doctorprofile=DoctorProfile::all();

        return view('blog',compact('blog','doctorprofile','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $blogs = blog::find($id);
       $comment_view= comment::all()
            ->where('status', '1')
             ;
//        $comment_view = comment::all()->where('blog_id','=',$id,'&&','status','=','1');
        return view('blogdetails',compact('blogs','comment_view'));
    }








    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
