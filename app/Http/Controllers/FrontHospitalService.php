<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class FrontHospitalService extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $Hospital=DB::table('hospital') ->orderBy('id', 'asc')->take(5)->get();
        return view('hospitalservice',compact('Hospital'));
    }
public function allindex()
    {
        $Hospital=DB::table('Hospital')->get();
        return view('hospitalservice',compact('Hospital'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {





        $Hospital= DB::table('hospital')->
            select('*')->where('id', $id)
            ->get();

          $doctorprofile=DB::table('doctor_hospitals')->select('doctor_profiles.name as name','doctor_profiles.photo as dphoto','doctor_profiles.id as id','hospital.photo','doctor_profiles.address as doctor_address','experience')
            ->leftJoin('doctor_profiles','doctor_profiles.id','doctor_hospitals.doctor_id')
            ->leftJoin('hospital','hospital.id','doctor_hospitals.hospital_id')
            ->where('doctor_hospitals.hospital_id','=',$id)->get();






        return view('Hospitalshow',compact('Hospital','doctorprofile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function allhospitaldoctor($id)
    {


        $Hospital= DB::table('Hospital')
        ->where('id', $id)
        ->first();

        $doctorprofile=DB::table('doctor_profiles')->select('name','hospital','id','speciality','phone as doctor_phone','doctor_profiles.address as doctor_address','speciality','member','awards','experience','photo')
            ->where('doctor_profiles.hospital',$id)->get();
        return view('hospitalshow',compact('Hospital','doctorprofile'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
