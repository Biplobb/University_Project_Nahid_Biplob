<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Donar;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\PDO;

class BloodDonoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $donors=Donar::select('id','name','blood_group','address','email')
            ->where ('status', '=', '1')
            ->paginate(10);

        $counters=DB::table('donars')->select(DB::raw('count(blood_group) as total,blood_group'))->groupBy('blood_group')->get();


        $district=DB::table('districts')->get();
        $upozila=DB::table('upazilas')->get();



       return view('blood',compact('district','donors','counters','upozila'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        return view('/blood');
    }


    public function store(Request $request)
    {


        $data=['name'=>$request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'date' => $request->date,
            'blood_group' => $request->blood_group,
            'email' => $request->email,
            'area' => $request->area,
            'upozila' => $request->upozila,
            'status' => $request->status];

        Donar::create($data);
        return redirect('/blood')->with('message','Your Blood Donate Request has been received. Thanks !') ;


    }


}
