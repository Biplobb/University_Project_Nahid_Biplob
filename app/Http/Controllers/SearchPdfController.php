<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DoctorProfile;
use App\Hospital;

use Carbon\Carbon;
use DB;
use Session;
use Redirect;
use Mail;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SearchPdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dropdownsearching(Request $request)
    {
        return $request;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {

        $results=array();

        $results=DB::table('patient')->select('*')
            ->where('name','like', '%' . $request->title. '%')
            ->orWhere('nid','like', '%' . $request->title. '%')->get();
          $patient=DB::table('patient')->select('*')
            ->where('name','like', '%' . $request->title. '%')
            ->orWhere('nid','like', '%' . $request->title. '%')->get();
  $re['id']=$request->re;
        if(empty($patient)){
            Session::put('exception','Patient name and id not found,please again try it');
            return redirect()->back();


        }

        if(!empty($patient)){

            return view('search.searching',compact('patient','results'));

        }

    }
  public function serialsearch(Request $request)
    {


        $results=array();
  $re=$request->re;

        $patient=DB::table('serial')->select('*')
              ->where('doctor_name','like', '%' . $request->title. '%')
              ->where('hospital_id','like', '%' . $request->re. '%')
              ->Where('date','like', '%' . $request->date. '%')->get();
        $re=$request->re;
       $a=sizeof($patient);

        if($a>0){

            return view('hospital-admin.serialseraching',compact('patient','re'));

        }
        else{

            Session::put('exception','Doctor name and date not found,please again try it');
            return view('hospital-admin.notfound',compact('patient','re'));
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
