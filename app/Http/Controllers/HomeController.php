<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DoctorProfile;
use App\Hospital;

use Carbon\Carbon;
use DB;
use Session;
use Redirect;
use Mail;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {

    }

    public function index()
    {
        return view('index');

    }
    public function logoutsuccess()
    {
        Session::put('exception','Successfully Logout');

        return view('index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createdoctor()
    {
        $data=DB::table('hospital')->select('id','name')->get();

        return view('registration.doctor',compact('data'));
    }
    public function storedoctor(Request $request)
    {

        $id='';
        if($request->hasFile('photo'))
        {
            $destinationPath="image/doctor-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            Image::make($file)->resize(200,200)->save($file->move($destinationPath,$filename));
        }
        $data=['name'=>$request->name,
            'speciality'=>$request->speciality,
            'degree'=>$request->degree,
            'email'=>$request->email,
            'password'=>$request->password,
            'phone'=>$request->phone,
            'address'=>$request->address,
            'member'=>$request->member,
            'awards'=>$request->awards,
            'experience'=>$request->experience,
            'photo'=>$filename];
        if(DoctorProfile::create($data))
        {
            $id=DoctorProfile::select('id')->orderBy('created_at','desc')->get()->first();
            for($i=0;$i<sizeof($request->hospital);$i++)
            {
                DB::table('doctor_hospitals')->insert(['doctor_id'=>$id->id,'hospital_id'=>$request->hospital[$i],'assigned'=>'no','first_fees'=>null,'second_fees'=>null,'created_at'=>Carbon::now(),'updated_at'=>Carbon::now()]);
                Session::put('exception','Successfully registered');
                return redirect()->back();

            }
        }

        Session::put('exception','again try you are not registered');
        return redirect()->back();
    }
    public function logindoctor(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

        $result = DB::table('doctor_profiles')
            ->where('email', $email)
            ->where('password', $password)
            ->first();

        $re= DB::table('doctor_profiles')->select('id')
            ->where('email', $email)
            ->where('password', $password)
            ->first();


        if($result){

            Session::put('id',$result->id);


            return view('doctor-admin.index');
        }else{

            Session::put('exception','Email or Password Invalid');
            return Redirect::to('/');
        }




    }

    public function createuser()
    {
        return view('registration.patient');
    }
    public function storepatient(Request $request)
    {
        if($request->hasFile('photo'))
        {
            $destinationPath="image/hospital-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));
        }
        $data=['name'=>$request->name,
            'email'=>$request->email,
            'password'=>$request->password,
            'address'=>$request->address,
            'phone'=>$request->phone,
            'nid'=>$request->nid,
            'occupation'=>$request->occupation,
            'photo'=>$filename];
        if ($success) {

            DB::table('patient')->insert(['name'=>$request->name,
                'email'=>$request->email,
                'password'=>$request->password,
                'address'=>$request->address,
                'phone'=>$request->phone,
                'nid'=>$request->nid,
                'occupation'=>$request->occupation,
                'status'=>'0',
                'photo'=>$filename,'created_at'=>now(),'updated_at'=>now()]);
            Session::put('exception','Successfully registered');
            return redirect()->back();

        }
        Session::put('exception','');
        return "again try you are not registered";
    }
    public function addreport($id){

        $re = DB::table('patient')
            ->select("*")
            ->where('id', $id)->first();
       return view('patient-admin.addreport',compact('re'));
    }
    public function addprescription($id){

        $re = DB::table('patient')
            ->select("*")
            ->where('id', $id)->first();
       return view('patient-admin.addprescription',compact('re'));
    }
    public function storereport(Request $request)
    {


        if($request->hasFile('photo'))
        {
            $destinationPath="image/hospital-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));
        }
     $data=['name'=>$request->name,
            'nid'=>$request->nid,

            'patient_id'=>$request->patient_id,

            'photo'=>$filename];
        if ($success) {

            DB::table('report')->insert(['name'=>$request->name,
                'nid'=>$request->nid,

                'patient_id'=>$request->patient_id,
                'active'=>'0',
                'photo'=>$filename,'created_at'=>now(),'updated_at'=>now()]);
            Session::put('exception','Successfully registered');
            return redirect()->back();

        }
        Session::put('exception','again try you are not registered');
        return redirect()->back();
    }
    public function allreport($id){

       $report = DB::table('report')
            ->select("*")
            ->where('patient_id', $id)->get();

        $a=sizeof($report);
        if($a>0){
            $re = DB::table('patient')
                ->select("*")
                ->where('id', $id)->get();
            $re =$id;
            return view('patient-admin.allreport',compact('re','report'));
        }
       else{
            $re =$id;
            return view('patient-admin.allreport',compact('re','report'));
        }

    }
    public function allprescription($id){
         $prescription = DB::table('prescription')
            ->select("*")
            ->where('patient_id', $id)->get();
        $re = DB::table('patient')
            ->select("*")
            ->where('id', $id)->first();
        return view('patient-admin.allprescription',compact('re','prescription'));
    }
    public function storeprescription(Request $request)
    {


        if($request->hasFile('photo'))
        {
            $destinationPath="image/hospital-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));
        }
     $data=['doctor_name'=>$request->name,


            'patient_id'=>$request->patient_id,

            'photo'=>$filename];
        if ($success) {

            DB::table('prescription')->insert(['doctor_name'=>$request->name,


                'patient_id'=>$request->patient_id,
                'active'=>'0',
                'photo'=>$filename,'created_at'=>now(),'updated_at'=>now()]);
            Session::put('exception','Successfully registered');
            return redirect()->back();

        }
        Session::put('exception','again try you are not registered');
        return redirect()->back();
    }
    public function loginpatient(Request $request)
    {

        $email = $request->email;
        $password = $request->password;

        $result = DB::table('patient')
            ->where('email', $email)
            ->where('password', $password)->first();
        $re= DB::table('patient')->select('id')
            ->where('email', $email)
            ->where('password', $password)
            ->first();
         if($result){

             Session::put('id',$result->id);


             return view('patient-admin.index',compact('re'));
             /*return Redirect::to('/admin');*/
         }else{

             Session::put('exception','Email or Password Invalid');
             return Redirect::to('/');
         }
    }
    public function createhospital()
    {
        return view('registration.hospital');
    }

    public function storehospital(Request $request)
    {

        if($request->hasFile('photo'))
        {
            $destinationPath="image/hospital-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));
        }
        $data=['name'=>$request->name,
            'location'=>$request->location,
            'service'=>$request->service,
            'review'=>$request->review,
            'contact'=>$request->contact,
            'about'=>$request->about,
            'email'=>$request->email,
            'password'=>$request->password,

            'photo'=>$filename];

        if ($success) {

            DB::table('hospital')->insert(['name'=>$request->name, 'contact'=>$request->contact,'review'=>$request->review,'service'=>implode(',',$request->service),'location'=>$request->location,'about'=>$request->about,'photo'=>$filename, 'email'=>$request->email,
                'password'=>$request->password,'created_at'=>now(),'updated_at'=>now()]);
            Session::put('exception','Successfully registered');
            return redirect()->back();
        }

        Session::put('exception','again try you are not registered');
        return redirect()->back();
    }
    public function hospitalinformationupdate($id)

    {
        $hospital = DB::table('hospital')
            ->where('id', $id)
            ->first();
        return view('registration.hospitalupdate',compact('hospital'));
    }

    public function hospitalinformationupdated(Request $request)
    {


        if($request->hasFile('photo'))
        {
            $destinationPath="image/hospital-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));
        }
        $data=['name'=>$request->name,
            'location'=>$request->location,
            'service'=>$request->service,
            'review'=>$request->review,
            'contact'=>$request->contact,
            'about'=>$request->about,
            'email'=>$request->email,
            'password'=>$request->password,

            'photo'=>$filename];

        if ($success) {

            DB::table('Hospital')->where('id', $request->id)->update(['name'=>$request->name, 'contact'=>$request->contact,'review'=>$request->review,'service'=>implode(',',$request->service),'location'=>$request->location,'about'=>$request->about,'photo'=>$filename, 'email'=>$request->email,
                'password'=>$request->password,'created_at'=>now(),'updated_at'=>now()]);
            Session::put('exception','Successfully UPDATED');
            return redirect()->back();
        }

        Session::put('exception','again try you are not registered');
        return redirect()->back();
    }
    public function hospitalserialadd($id)
    {
        $ids=$id;
        $hospital=DB::table('hospital')->where('id', $ids)->select("*")->first();



        $doctor=DB::table('doctor_hospitals')->select('doctor_hospitals.hospital_id as hospital_id','doctor_hospitals.doctor_id as doctor_id','doctor_profiles.name as doctorname','doctor_profiles.id as doctorid','hospital.name as hospitalname','hospital.id as hospitalid')
            ->leftJoin('doctor_profiles','doctor_profiles.id','=','doctor_hospitals.doctor_id')
            ->leftJoin('hospital','hospital.id','=','doctor_hospitals.hospital_id')
            ->where('hospital.id','=',$ids)
            ->distinct()
            ->get();


         $a=sizeof($doctor);
        return view('registration.createserial',compact('hospital','doctor','a'));
    }
    public function hospitalserialappointment($id)
    {
         $ids=$id;
         $re=$ids;




         $doctor=DB::table('doctor_hospitals')->select('serial.phone as serialphone','serial.doctor_name as serialdoctor_name','serial.date as serialdate','serial.name as serialname','doctor_hospitals.hospital_id as hospital_id','doctor_hospitals.doctor_id as doctor_id','doctor_profiles.name as doctorname','doctor_profiles.id as doctorid','hospital.name as hospitalname','hospital.id as hospitalid')
            ->leftJoin('doctor_profiles','doctor_profiles.id','=','doctor_hospitals.doctor_id')
            ->leftJoin('hospital','hospital.id','=','doctor_hospitals.hospital_id')
            ->leftJoin('serial','serial.hospital_id','=','doctor_hospitals.hospital_id')
            ->groupby('serial.date')
            ->groupby('serial.date')
            ->groupby('serial.doctor_name')
            ->where('hospital.id','=',$ids)
            ->OrderBy('serial.doctor_name')
            ->distinct()
            ->get();


         $a=sizeof($doctor);
        return view('registration.allappointment',compact('doctor','a','re'));
    }

    public function storehospitalserial(Request $request)
    {

        $data=['hospital_id'=>$request->hospital_id,
            'date'=>$request->date,
            'name'=>$request->name,
            'phone'=>$request->phone,
            'doctor_name'=>$request->doctor_name,
        ];



        DB::table('serial')->insert($data);
            Session::put('exception','Successfully inserted');
            return redirect()->back();

        return view('registration.createserial',compact('hospital'));
    }
    public function loginhospital(Request $request)
    {

        $email = $request->email;
        $password = $request->password;

        $result = DB::table('hospital')
            ->where('email', $email)
            ->where('password', $password)
            ->first();

        $re= DB::table('hospital')
            ->where('email', $email)
            ->where('password', $password)
            ->first();
        if($result){

            Session::put('id',$result->id);

            return view('hospital-admin.index',compact('re'));
            /*return Redirect::to('/admin');*/
        }else{

            Session::put('exception','Email or Password Invalid');
            return Redirect::to('/');
        }

    }
    public function createtourism()
    {
        return 'createtourism';
    }
    public function creatediagnostic()
    {
        return view('registration.diagnostic');
    }
    public function storediagnostic(Request $request)
    {
        if($request->hasFile('photo'))
        {
            $destinationPath="image/diagnostic-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(200,200)->save($file->move($destinationPath,$filename));
        }
        $data=['name'=>$request->name,
            'location'=>$request->location,
            'email'=>$request->email,
            'password'=>$request->password,
            'phone'=>$request->phone,
            'about'=>$request->phone,

            'photo'=>$filename];
        if ($success) {

            DB::table('diagnostic_center')->insert(['name'=>$request->name,
                'location'=>$request->location,
                'email'=>$request->email,
                'password'=>$request->password,
                'phone'=>$request->phone,
                'about'=>$request->phone,

                'photo'=>$filename,'created_at'=>now(),'updated_at'=>now()]);
            Session::put('exception','Successfully registered');
            return redirect()->back();

        }
        Session::put('exception','again try you are not registered');
        return redirect()->back();

    }
    public function logindiagnostic(Request $request)
    {

        $email = $request->email;
        $password = $request->password;

        $result = DB::table('diagnostic_center')
            ->where('email', $email)
            ->where('password', $password)
            ->first();
        $re= DB::table('diagnostic_center')
            ->where('email', $email)
            ->where('password', $password)
            ->first();

        if($result){

            Session::put('id',$result->id);

            return view('diagnostic-admin.index',compact('re'));
            /*return Redirect::to('/admin');*/
        }else{

            Session::put('exception','Email or Password Invalid');
            return Redirect::to('/');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function contactstore(Request $request)
    {
        $name=$request->name;
        $email=$request->email;
        $phone=$request->phone;
        $message=$request->message;

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;

        $data['message'] = $request->message;


        $success =   DB::table('contact')->insert($data);

        Mail::send('mail', $data, function ($message) use ($data) {

            $message->to($data['email']);
            $message->from('ijirkcom@gmail.com','ijirkcom@gmail.com');
            $message->subject('From Deshidoctor');
            Session::put('message', 'Your Message Sent Successfully!!');
            return redirect()->back();

        });

        Mail::send('mailtemplate', $data, function ($message) use ($data) {
            $message->from($data['email'])->subject($data['email']);
            $message->to('ijirkcom@gmail.com');


        });











        Session::put('message', 'Your Message Sent Successfully!!');
        return redirect()->back();



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function interview()
    {
        return view('interview');
    }
    public function shop()
    {
        return view('shop');
    }


    public function equipment()
    {
        return view('equipment');
    }
    public function india()
    {
        return view('india');
    }


    public function about(){
        return view('about');
    }

    public function appointment(){
        return view('appointment');
    }

    public function faq(){
        return view('faq');
    }

    public function services(){
        return view('services');
    }
    public function doctors(){

        return view('doctors');
    }
    public function features(){
        return view('features');
    }
    public function blog(){
        return view('blog');
    }
    public function awards(){
        return view('awards');
    }

    public function contacts(){
        return view('contact');
    }

    public function others(){
        return view('others');
    }

    public function generalhospital(){

        return view('generalhospital');
    }
    public function dentalhospital(){
        return view('dental');
    }

}
