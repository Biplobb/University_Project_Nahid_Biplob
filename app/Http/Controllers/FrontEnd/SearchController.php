<?php

namespace App\Http\Controllers\FrontEnd;

use App\DoctorProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function searchdoctor(Request $request){
        $option ='';
        $data=DoctorProfile::select('name','id','degree')
            ->where('name','like','%'.$request->keyword.'%')->get();

        $option .="<ul class='datalist'>";
        if(sizeof($data)>0){
            foreach ($data as $d)
            {
                $option .="<li class='lists'  data-id='$d->id' data-name='$d->name'>".$d->name."<p>$d->degree"."$d->degree</p></li>";
            }
        }else{
            $option .="<li>No data found</li>";
        }
        $option .="</ul>";
        return $option;
    }



    public function searchresult(Request $request)
    {

        if($request->lookingfor=='blood' &&
            ($request->keyword=='A+'
                || $request->keyword=='A-'
                || $request->keyword=='O+'
                || $request->keyword=='O-'
                || $request->keyword=='AB+'
                || $request->keyword=='AB-' || $request->keyword=='B+' || $request->keyword=='B-')){
            return redirect('blood/'.$request->keyword);
        }else if($request->lookingfor=='doctor' && $request->keyword!='' && $request->dctid!=''){
            $data=DoctorProfile::select('id')
                ->where('id','=',$request->dctid)
                ->where('name','=',$request->keyword)->get()->first();
            return redirect('profile/'.$data->id);
        }else{
            return view('404');
        }


    }
}
