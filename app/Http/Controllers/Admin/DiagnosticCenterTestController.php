<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use App\tests;
use Illuminate\Http\Request;

class DiagnosticCenterTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }

    public function index()
    {
        $diagnostic_center_tests=DB::table('diagnostic_center_tests')->get();

        return view('admin.diagnostic.diagnostic-center-test.index',compact('diagnostic_center_tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tests = tests::all(['id', 'name']);
         $diagnostic_center=DB::table('diagnostic_center')->get();

        return view('admin.diagnostic.diagnostic-center-test.create', compact('tests','diagnostic_center'));

        /* return view('admin.diagnostic.diagnostic-center-test.create',compact('diagnostic_center'));*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

 for($i=0;$i<sizeof($request->tests); $i++){
    DB::table('diagnostic_center_tests')->insert(['previous'=>$request->previous[$i] ,'discount'=>$request->discount[$i],'discountprice'=>$request->previous[$i]-(($request->previous[$i])*($request->discount[$i]/100)),'Tests_id'=>$request->tests[$i] ,'Diagnostic_Center_id'=>$request->Diagnostic_Center_id,'created_at'=>now(),'updated_at'=>now()]);

 }

      return redirect('admin/diagnostic/diagnostic-center-test/');





    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $diagnostic_center_tests= DB::table('diagnostic_center_tests')
            ->where('id', $id)
            ->first();

        return view('admin.diagnostic.diagnostic-center-test.edit')->with('diagnostic_center_tests', $diagnostic_center_tests);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array();
        $id = $request->id;
        $data['Tests_id'] = $request->Tests_id;
        $data['Diagnostic_Center_id'] = $request->Diagnostic_Center_id;


        DB::table('diagnostic_center_tests')
            ->where('id', $id)
            ->update($data);





        return redirect('admin/diagnostic/diagnostic-center-test/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('diagnostic_center_tests')
            ->where('id', $id)
            ->delete();


        return redirect('admin/diagnostic/diagnostic-center-testull/');
    }
}
