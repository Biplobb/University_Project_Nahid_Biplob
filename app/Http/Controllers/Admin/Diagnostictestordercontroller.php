<?php


namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use App\tests;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Mail;
use Carbon\Carbon;
class Diagnostictestordercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /*public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }*/
    public function index()
    {
       /* $user_info = DB::table('usermetas')
            ->select('browser', DB::raw('count(*) as total'))
            ->groupBy('browser')
            ->get();*/
      $diagnostic_center= DB::table('_testorder')->select('_testorder.name as testordername','_testorder.id as testorderid','_testorder.address as testorderaddress','_testorder.phone as testorderphone','tests.name as testsname','tests.price as testsprice')-> leftJoin('tests','_testorder.Tests_id','tests.id')
           ->get();
       return view('admin.Testorder.index',compact('diagnostic_center'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['address'] = $request->address;

        $data['phone'] = $request->phone;

        if($request->hasFile('photo'))
        {
            $destinationPath="image/diagnostic-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(200,200)->save($file->move($destinationPath,$filename));
        }

        $data=['photo'=>$filename];


        for($i=0;$i<sizeof($request->tests); $i++){
            DB::table('_testorder')->insert(['Tests_id'=>$request->tests[$i] ,'name'=>$request->name,'photo'=>$filename,'email'=>$request->email,'address'=>$request->address,'phone'=>$request->phone,'created_at'=>now(),'updated_at'=>now()]);

        }
        Mail::send('prescription', $data, function ($message) use ($data) {
            $message->from($data['email'])->subject($data['email']);
            $message->to('ijirkcom@gmail.com');


        });

        Mail::send('mail', $data, function ($message) use ($data) {

            $message->to($data['email']);

            $message->from('ijirkcom@gmail.com','ijirkcom@gmail.com');
            $message->subject('From Deshidoctor');
            Session::put('message', 'Your Message Sent Successfully!!');
            return redirect()->back();

        });




        Session::put('message', 'Your Prescription photo successfully Sent,we will contact with your email!!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('_testorder')
            ->where('id', $id)
            ->delete();


        return redirect('admin/testorder/');
    }
}
