<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AmbulanceController extends Controller
{
    public function index()
    {
        return view('admin.ambulance.index');
    }

    public function create()
    {
        return view('admin.ambulance.create');
    }

    public function show()
    {
        return view('admin.ambulance.show');
    }
}
