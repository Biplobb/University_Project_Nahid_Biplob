<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }

    public function index()
    {

        $Hospital=DB::table('Hospital')->get();
        return view('admin.hospital.index',compact('Hospital'));

      /*  return view('admin.hospital.index',compact('hospital_service'));*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.hospital.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request )
    {

        if($request->hasFile('photo'))
        {
            $destinationPath="image/hospital-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));
        }
        $data=['name'=>$request->name,
            'location'=>$request->location,
            'service'=>$request->service,
            'review'=>$request->review,
            'contact'=>$request->contact,
            'about'=>$request->about,
            'email'=>$request->email,
            'password'=>$request->password,

            'photo'=>$filename];
        if ($success) {

            DB::table('Hospital')->insert(['name'=>$request->name, 'contact'=>$request->contact,'review'=>$request->review,'service'=>implode(',',$request->service),'location'=>$request->location,'about'=>$request->about,'photo'=>$filename, 'email'=>$request->email,
                'password'=>$request->password,'created_at'=>now(),'updated_at'=>now()]);
            return redirect('admin/hospital/index/');
        }





    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Hospital= DB::table('Hospital')
            ->where('id', $id)
            ->first();

        return view('admin.hospital.edit')->with('Hospital', $Hospital);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $data = array();
//        $id = $request->id;
//        $data['name'] = $request->name;
//        $data['location'] = $request->location;
//        $data['about'] = $request->about;
//        $data['location'] = $request->location;




        DB::table('Hospital')->update(['name'=>$request->name,'service'=>implode(',',$request->service),'location'=>$request->location,'photo'=>$request->photo,'about'=>$request->about,'created_at'=>now(),'updated_at'=>now()]);
        return redirect('admin/hospital/index/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('Hospital')
            ->where('id', $id)
            ->delete();


        return redirect('admin/hospital/index');
    }
}
