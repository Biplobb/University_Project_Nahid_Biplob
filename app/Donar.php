<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donar extends Model
{
    protected $fillable=['name','address','phone','date','blood_group','email','area','upozila','status'];
}
