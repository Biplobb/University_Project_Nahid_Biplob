<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seaker extends Model
{
    protected $fillable=['name','hname','haddress','relationship','phone','email','donor_id'];
}
