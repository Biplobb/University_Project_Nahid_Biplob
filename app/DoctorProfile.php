<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class DoctorProfile extends Authenticatable
{

    use Uuids;
    public $incrementing = false;
    protected $guard='doctor';
    protected $fillable=['name','speciality','degree','password','email','phone','address','hospital','member','awards','experience','photo'];
}
