<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blog extends Model
{
    protected $fillable=['title','subtitle','author','blog_post','type','image','doctor_id'];
}
